import express, { json } from 'express';
import morgan from 'morgan';
import mongoose from 'mongoose';
import { config } from 'dotenv';
import cors from 'cors';
import bodyParser from 'body-parser';
import { authRouter } from './src/routers/authRouter.js';
import { usersRouter } from './src/routers/usersRouter.js';
import { loadsRouter } from './src/routers/loadsRouter.js';
import { authMiddleware } from './src/middleware/authMiddleware.js';
import { asyncWrapper } from './src/middleware/asyncWrapper.js';
import { trucksRouter } from './src/routers/trucksRouter.js';

const app = express();

config();

const { PORT } = process.env;

app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());
app.use(json());
app.use(morgan('tiny'));

mongoose
  .connect(process.env.DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
  }))
  .catch((err) => {
    throw new Error(err.message);
  });

app.use('/api/auth', authRouter);
app.use('/api/users', asyncWrapper(authMiddleware), usersRouter);
app.use('/api/loads', asyncWrapper(authMiddleware), loadsRouter);
app.use('/api/trucks', asyncWrapper(authMiddleware), trucksRouter);

function errorHandler(err, req, res) {
  return res.status(400).send({ message: 'Bad request' });
}

app.use(errorHandler);
