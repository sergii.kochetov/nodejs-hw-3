import { Router } from 'express';
import {
  userData,
  deleteUser,
  changeUserPassword,
} from '../controllers/usersController.js';
import { asyncWrapper } from '../middleware/asyncWrapper.js';

const router = Router();

router.get('/me', asyncWrapper(userData));

router.delete('/me', asyncWrapper(deleteUser));

router.patch('/me/password', asyncWrapper(changeUserPassword));

export const usersRouter = router;
