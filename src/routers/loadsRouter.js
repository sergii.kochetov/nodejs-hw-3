import { Router } from 'express';
import { asyncWrapper } from '../middleware/asyncWrapper.js';
import {
  loadNew,
  loadPost,
  loadsAll,
  loadActive,
  loadShipperUpdate,
  loadGetById,
  loadDelete,
  shippingInfo,
  loadUpdateState,
} from '../controllers/loadsController.js';

const router = Router();

router.get('/', asyncWrapper(loadsAll));

router.post('/', asyncWrapper(loadNew));

router.get('/active', asyncWrapper(loadActive));

router.patch('/active/state', asyncWrapper(loadUpdateState));

router.get('/:id', asyncWrapper(loadGetById));

router.put('/:id', asyncWrapper(loadShipperUpdate));

router.delete('/:id', asyncWrapper(loadDelete));

router.post('/:id/post', asyncWrapper(loadPost));

router.get('/:id/shipping_info', asyncWrapper(shippingInfo));

export const loadsRouter = router;
