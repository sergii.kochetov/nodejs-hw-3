import jwt from 'jsonwebtoken';

import { findUserByParam } from '../services/usersService.js';

const authMiddleware = async (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    return res
      .status(400)
      .json({ message: 'Please provide authorization header' });
  }

  const [, token] = authorization.split(' ');

  if (!token) {
    return res
      .status(400)
      .json({ message: 'Please include token to the request' });
  }

  const tokenPayload = jwt.verify(token, process.env.S3_API);

  await findUserByParam({ _id: tokenPayload.userId });

  req.user = {
    _id: tokenPayload.userId,
    role: tokenPayload.role,
    email: tokenPayload.email,
    created_date: tokenPayload.created_date,
  };

  next();
};

export { authMiddleware };
