import bcryptjs from 'bcryptjs';
import jwt from 'jsonwebtoken';
import { User, userJoiSchema } from '../models/User.js';
import {
  validateSpecificField,
  generatePassword,
  // sendPasswordByEmail,
} from '../services/utilService.js';

import {
  saveUser,
  findUserByParam,
  comparePasswords,
} from '../services/usersService.js';

const { hash } = bcryptjs;

const registerUser = async (req, res) => {
  const { role, email, password } = req.body;

  const isvalidated = await userJoiSchema.validateAsync({
    role,
    email,
    password,
  });
  console.log(isvalidated);
  const isUserExisting = User.findOne({ email });

  if (!isUserExisting) {
    throw new Error('User with current email is already registered');
  }

  saveUser({ role, email, password })
    .then(() => res.status(200).json({
      message: 'Profile created successfully',
    }))
    .catch(() => {
      res.status(500).json({
        message: 'Internal server error',
      });
    });
};

const loginUser = async (req, res) => {
  const { email, password } = req.body;

  await validateSpecificField({ email }, userJoiSchema);
  await validateSpecificField({ password }, userJoiSchema);

  const user = await findUserByParam({ email });

  await comparePasswords(password, user.password);

  const payload = {
    email: user.email,
    userId: user._id,
    role: user.role,
    created_date: user.created_date,
  };

  const jwtToken = jwt.sign(payload, process.env.S3_API);

  return res.status(200).json({
    jwt_token: jwtToken,
  });
};

const forgotPassword = async (req, res) => {
  const { email } = req.body;

  await validateSpecificField({ email }, userJoiSchema);

  const user = await findUserByParam({ email });

  const generatedPassword = generatePassword();

  await User.findByIdAndUpdate(user._id, {
    $set: { password: await hash(generatedPassword, 10) },
  });

  console.log(generatedPassword);
  // await sendPasswordByEmail(email, generatedPassword);

  return res.status(200).json({
    message: 'New password sent to your email address',
  });
};

export { registerUser, loginUser, forgotPassword };
