const truckData = [
  {
    type: 'SPRINTER',
    loadData: {
      payload: 1700,
      dimensions: {
        length: 300,
        width: 250,
        height: 170,
      },
    },
  },
  {
    type: 'SMALL STRAIGHT',
    loadData: {
      payload: 2500,
      dimensions: {
        length: 500,
        width: 250,
        height: 170,
      },
    },
  },
  {
    type: 'LARGE STRAIGHT',
    loadData: {
      payload: 4000,
      dimensions: {
        length: 700,
        width: 350,
        height: 200,
      },
    },
  },
];

const isDriver = (role) => {
  if (role === 'DRIVER') {
    return true;
  }
  throw new Error("Available only for driver's role");
};

export { truckData, isDriver };
