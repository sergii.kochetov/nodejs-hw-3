import mongoose from 'mongoose';
import { Truck, truckJoiSchema } from '../models/Truck.js';
import { truckData, isDriver } from '../services/truckServices.js';
import { validateSpecificField } from '../services/utilService.js';

const truckNew = async (req, res) => {
  const { type } = req.body;
  const { role, _id, created_date } = req.user;

  isDriver(role);

  if (!type) {
    throw new Error('Please specify the type of the truck');
  }

  await validateSpecificField({ type }, truckJoiSchema);

  const { loadData } = truckData.find((truck) => truck.type === type);

  const { payload, dimensions } = loadData;

  const truck = new Truck({
    created_by: new mongoose.mongo.ObjectId(_id),
    type,
    created_date,
    payload,
    dimensions,
  });

  await truck.save();

  return res.status(200).json({ message: 'Truck created successfully' });
};

const trucksAll = async (req, res) => {
  const { role } = req.user;
  isDriver(role);
  const trucks = await Truck.find();

  if (trucks.length === 0) {
    return res.status(200).json({ message: 'No trucks found in the database' });
  }

  return res.status(200).json({ trucks });
};

const truckPersonal = async (req, res) => {
  const { id: _id } = req.params;
  const { role } = req.user;
  isDriver(role);

  const truck = await Truck.findOne({ _id });

  if (!truck) {
    throw new Error('No truck with such ID found');
  }

  return res.status(200).json({ truck });
};

const truckPersonalTypeChange = async (req, res) => {
  const { id: _id } = req.params;
  const { role, _id: userId } = req.user;
  const { type } = req.body;

  isDriver(role);

  if (!type) {
    throw new Error('Please specify the type of the truck');
  }

  await validateSpecificField({ type }, truckJoiSchema);

  const {
    type: existingType,
    created_by,
    status,
  } = await Truck.findOne({ _id });

  if (created_by.toString() !== userId) {
    throw new Error("Only the owner can change this truck's type");
  } else if (status === 'OL') {
    throw new Error('You have an active delivery. Please finish it up first.');
  } else if (type === existingType) {
    throw new Error(`This truck is already the ${type}`);
  }

  const { loadData } = truckData.find((truck) => truck.type === type);

  const { payload, dimensions } = loadData;

  const truck = await Truck.findOneAndUpdate(
    { _id },
    { type, payload, dimensions },
  );

  if (!truck) {
    throw new Error('No truck with such ID found');
  }

  return res
    .status(200)
    .json({ message: 'Truck details changed successfully' });
};

const truckDelete = async (req, res) => {
  const { id: _id } = req.params;
  const { role, _id: userId } = req.user;

  isDriver(role);

  const truck = await Truck.findOne({ _id });

  if (!truck) {
    throw new Error('No truck with such ID found');
  } else if (truck.created_by.toString() !== userId) {
    throw new Error('Only the owner can delete the info about this truck');
  } else if (truck.status === 'OL') {
    throw new Error('You have an active delivery. Please finish it up first.');
  }

  await Truck.findByIdAndDelete(_id);

  return res.status(200).json({ message: 'Truck deleted successfully' });
};

const truckAssignById = async (req, res) => {
  const { id: truckId } = req.params;
  const { role, _id: userId } = req.user;

  isDriver(role);

  const truckToAssign = await Truck.findOne({ _id: truckId });

  if (truckToAssign.created_by.toString() !== userId) {
    throw new Error("You aren't the owner of the truck. You can't assign it");
  } else if (truckToAssign.assigned_to !== null) {
    throw new Error('This truck has already been assigned');
  }

  await Truck.findOneAndUpdate({ _id: truckId }, { assigned_to: userId });

  return res.status(200).json({ message: 'Truck assigned successfully' });
};

export {
  truckNew,
  trucksAll,
  truckPersonal,
  truckPersonalTypeChange,
  truckDelete,
  truckAssignById,
};
