import mongoose from 'mongoose';
import Joi from 'joi';

const truckJoiSchema = Joi.object({
  // created_by: Joi.string().hex(),
  // assigned_to: Joi.string().hex(),
  status: Joi.string().pattern(/^(IS|OL)$/),
  type: Joi.string().pattern(/^(SPRINTER|SMALL\sSTRAIGHT|LARGE\sSTRAIGHT)$/),
  // name: Joi.string().required(),
  // payload: Joi.number().required(),
  // dimensions: Joi.object({
  //   width: Joi.number().required(),
  //   length: Joi.number().required(),
  //   height: Joi.number().required(),
  // }),
});

const truckSchema = new mongoose.Schema(
  {
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    assigned_to: {
      type: mongoose.Schema.Types.ObjectId,
      default: null,
    },
    status: {
      type: String,
      default: 'IS',
      required: true,
    },
    type: {
      type: String,
      required: true,
    },
    payload: {
      type: String,
      required: true,
    },
    dimensions: {
      width: {
        type: Number,
        required: true,
      },
      length: {
        type: Number,
        required: true,
      },
      height: {
        type: Number,
        required: true,
      },
    },
  },
  {
    timestamps: {
      createdAt: 'created_date',
      updatedAt: false,
    },
    versionKey: false,
  },
);

const Truck = mongoose.model('Truck', truckSchema);

export { Truck, truckJoiSchema };
