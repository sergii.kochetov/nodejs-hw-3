import { Router } from 'express';
import {
  registerUser,
  loginUser,
  forgotPassword,
} from '../controllers/authController.js';
import { asyncWrapper } from '../middleware/asyncWrapper.js';

const router = Router();

router.post('/register', asyncWrapper(registerUser));

router.post('/login', asyncWrapper(loginUser));

router.post('/forgot_password', asyncWrapper(forgotPassword));

export const authRouter = router;
