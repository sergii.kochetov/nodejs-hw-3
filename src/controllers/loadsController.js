import mongoose from 'mongoose';
import { Load, loadJoiSchema } from '../models/Load.js';
import { isShipper, compareLoadParams } from '../services/loadsService.js';
import { Truck } from '../models/Truck.js';
import { isDriver } from '../services/truckServices.js';

const loadNew = async (req, res) => {
  const { role } = req.user;

  isShipper(role);

  await loadJoiSchema.validateAsync(req.body);

  const load = new Load({
    created_by: req.user._id,
    status: 'NEW',
    ...req.body,
  });

  await load.save();

  return res.status(200).json({ message: 'Load created successfully' });
};

const loadPost = async (req, res) => {
  const { role } = req.user;
  const { id: loadId } = req.params;

  isShipper(role);

  const load = await Load.findOne({ _id: loadId });

  if (!load) {
    return res.status(200).json({ message: 'No load with such ID' });
  }

  const trucks = await Truck.find();

  const truck = trucks.filter((truckItem) => {
    if (truckItem.assigned_to !== null && truckItem.status === 'IS') {
      return truckItem;
    }
  });

  if (truck.length === 0) {
    return res.status(400).json({ message: 'No available truck found' });
  }
  for (let i = 0; i < truck.length; i++) {
    if (compareLoadParams(truck[i], load)) {
      const loadLogs = load.logs;

      await Load.findOneAndUpdate(
        { _id: loadId },
        {
          assigned_to: new mongoose.mongo.ObjectId(truck[i]._id),
          status: 'ASSIGNED',
          state: 'En route to Pick Up',
          logs: {
            ...loadLogs,
            message: `Truck has been found. Driver with id "${truck[i].created_by}" will reach out to you shortly.`,
          },
        },
      );

      await Truck.findOneAndUpdate(
        { _id: truck[i]._id },
        {
          status: 'OL',
        },
      );

      return res
        .status(200)
        .json({ message: 'Load posted successfully', driver_found: true });
    }
  }
};

const loadsAll = async (req, res) => {
  const { offset = 0, limit = 10 } = req.query;
  const { role, _id } = req.user;

  const loads = await Load.find();

  if (loads.length === 0) {
    return res.status(200).json({ message: 'No loads found in the database' });
  }

  if (role === 'SHIPPER') {
    return res.status(200).json({
      offset,
      limit,
      count: loads.length,
      loads: loads.slice(offset, limit || loads.length),
    });
  }
  if (role === 'DRIVER') {
    const driversLoads = loads.filter((item) => {
      if (item.assigned_to !== null && item.assigned_to.toString() === _id) {
        return item;
      }
    });

    if (driversLoads.length !== 0) {
      return res.status(200).json({
        offset,
        limit,
        count: driversLoads.length,
        loads: driversLoads.slice(offset, limit || driversLoads.length),
      });
    }
    return res.status(200).json({
      message:
        "Mr. Driver, you don't have any loads either completed or assigned to you",
    });
  }
  throw new Error('Bad request');
};

const loadActive = async (req, res) => {
  const { role, _id } = req.user;

  isDriver(role);

  const truckAssigned = await Truck.findOne({ assigned_to: _id });

  if (!truckAssigned) {
    throw new Error('Mr. Driver, please assign a truck first');
  }

  const loadAssigned = await Load.findOne({
    assigned_to: truckAssigned._id,
  });

  if (!loadAssigned) {
    throw new Error("Mr. Driver, you don't have any active loads yet");
  }

  return res.status(200).json({
    load: loadAssigned,
  });
};

const loadShipperUpdate = async (req, res) => {
  const { id: loadId } = req.params;
  const { role } = req.user;

  isShipper(role);

  await loadJoiSchema.validateAsync(req.body);

  await Load.findByIdAndUpdate(loadId, req.body);

  return res.status(200).json({
    message: 'Load details changed successfully',
  });
};

const loadGetById = async (req, res) => {
  const { id: loadId } = req.params;
  const { role } = req.user;

  isShipper(role);

  const load = await Load.findOne({ _id: loadId });

  return res.status(200).json({ load });
};

const loadDelete = async (req, res) => {
  const { id: _id } = req.params;
  const { role, _id: shipperId } = req.user;

  isShipper(role);

  const load = await Load.findOne({ _id });

  if (load.created_by.toString() !== shipperId) {
    return res
      .status(400)
      .json({ message: 'You are not allowed to delete this load' });
  }
  if (load.status !== 'NEW') {
    return res.status(400).json({ message: "This load can't be deleted" });
  }

  await Load.findByIdAndDelete({ _id });

  return res.status(200).json({ message: 'Load deleted successfully' });
};

const shippingInfo = async (req, res) => {
  const { id: _id } = req.params;
  const { role, _id: shipperId } = req.user;

  isShipper(role);

  const load = await Load.findOne({ _id });

  if (load.created_by.toString() !== shipperId) {
    return res.status(400).json({
      message: 'You are not allowed to see the shipping info of this load',
    });
  }
  if (load.status === 'NEW') {
    return res.status(400).json({ message: "This load has a 'NEW' status" });
  }

  const truck = Truck.findOne({ assigned_to: load._id });

  return res.status(200).json({ load, truck });
};

const loadUpdateState = async (req, res) => {
  const { role, _id: driverId } = req.user;

  isDriver(role);

  const truckAssigned = await Truck.findOne({ assigned_to: driverId });

  if (!truckAssigned) {
    throw new Error('Mr. Driver, please assign a truck first');
  }

  const load = await Load.findOne({
    assigned_to: truckAssigned._id,
  });

  const states = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
  ];

  if (!load) {
    return res.status(400).json({
      message: "You don't have any loads assigned to you",
    });
  }
  const { state, status } = load;

  if (status === 'SHIPPED') {
    return res.status(400).json({
      message: 'This load has already been shipped',
    });
  }
  const currentState = states.indexOf(state);

  const nextState = states[currentState + 1];

  if (nextState === states[states.length - 1]) {
    await Load.findByIdAndUpdate(
      { _id: load._id },
      { state: `${nextState}`, status: 'SHIPPED' },
    );
  } else {
    await Load.findByIdAndUpdate({ _id: load._id }, { state: `${nextState}` });
  }

  return res
    .status(200)
    .json({ message: `Load state changed to ${nextState}` });
};

export {
  loadNew,
  loadsAll,
  loadActive,
  loadShipperUpdate,
  loadPost,
  loadGetById,
  loadDelete,
  shippingInfo,
  loadUpdateState,
};
