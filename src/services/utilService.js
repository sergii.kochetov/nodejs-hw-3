import nodemailer from 'nodemailer';
import generator from 'generate-password';

const generatePassword = () => generator.generate({
  length: 5,
  numbers: true,
});

const sendPasswordByEmail = async (email, password) => {
  const transporter = nodemailer.createTransport({
    service: process.env.SERVICE,
    auth: {
      user: process.env.USER,
      pass: process.env.PASS,
    },
  });

  console.log(password);

  const info = await transporter.sendMail({
    from: process.env.USER,
    to: email,
    subject: 'Reset password',
    text: password,
  });

  console.log('Message sent: %s', info.messageId);
  console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
};

const validateSpecificField = async (fieldObj, schema) => {
  const [fieldKey, fieldValue] = Object.entries(fieldObj)[0];

  return schema.extract([`${fieldKey}`]).validateAsync(fieldValue);
};

export { sendPasswordByEmail, generatePassword, validateSpecificField };
