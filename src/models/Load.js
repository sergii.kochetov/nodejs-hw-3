import mongoose from 'mongoose';
import Joi from 'joi';

const loadJoiSchema = Joi.object({
  // created_by: Joi.string().hex(),
  // assigned_to: Joi.string().hex(),
  // logs: Joi.array().items(
  //   Joi.object({
  //     message: Joi.string().required(),
  //     time: Joi.date().required(),
  //   })
  // ),
  // status: Joi.string().pattern(/\b(NEW|POSTED|ASSIGNED|SHIPPED)\b/),
  // state: Joi.string(),

  name: Joi.string().required(),
  payload: Joi.number().required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: Joi.object({
    width: Joi.number().required(),
    length: Joi.number().required(),
    height: Joi.number().required(),
  }),
});

const loadSchema = new mongoose.Schema(
  {
    created_by: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
    },
    assigned_to: {
      type: mongoose.Schema.Types.ObjectId,
      default: null,
    },
    logs: {
      message: {
        type: String,
        default: 'New load created',
        required: true,
      },
      time: {
        type: Date,
        default: Date.now,
        required: true,
      },
      driverId: {
        type: String,
        required: false,
      },
    },
    status: {
      type: String,
      default: 'NEW',
      required: true,
    },
    state: {
      type: String,
      default: '',
    },
    name: {
      type: String,
      required: true,
    },
    payload: {
      type: Number,
      required: true,
    },
    pickup_address: {
      type: String,
      required: true,
    },
    delivery_address: {
      type: String,
      required: true,
    },
    dimensions: {
      width: {
        type: Number,
        required: true,
      },
      length: {
        type: Number,
        required: true,
      },
      height: {
        type: Number,
        required: true,
      },
    },
  },
  {
    timestamps: {
      createdAt: 'created_date',
      updatedAt: false,
    },
    versionKey: false,
  },
);

const Load = mongoose.model('Load', loadSchema);

export { Load, loadJoiSchema };
