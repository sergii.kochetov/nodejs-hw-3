import bcryptjs from 'bcryptjs';
import { User } from '../models/User.js';
import { comparePasswords } from '../services/usersService.js';

const { hash } = bcryptjs;

const userData = async (req, res) => res.status(200).json({ user: req.user });

const deleteUser = async (req, res) => {
  await User.findByIdAndDelete(req.user._id);
  return res.status(200).json({ message: 'Profile deleted successfully' });
};

const changeUserPassword = async (req, res) => {
  const { oldPassword, newPassword } = req.body;
  const { _id } = req.user;

  const { password: currentPassword } = await User.findById(_id);

  await comparePasswords(oldPassword, currentPassword);

  if (oldPassword === newPassword) {
    throw new Error("New password can't be the same as the old one");
  }

  await User.findByIdAndUpdate(_id, {
    $set: { password: await hash(newPassword, 10) },
  });

  return res.status(200).json({ message: 'Password changed successfully' });
};

export { userData, deleteUser, changeUserPassword };
