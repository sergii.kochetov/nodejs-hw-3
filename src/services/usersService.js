import bcryptjs from 'bcryptjs';
import { User } from '../models/User.js';

const { hash, compare } = bcryptjs;

const saveUser = async ({ role, email, password }) => {
  const user = new User({
    role,
    email,
    password: await hash(password, 10),
  });

  return user.save();
};

const findUserByParam = async (param) => {
  const [paramKey] = Object.keys(param);
  const user = await User.findOne(param);
  if (!user) {
    throw new Error(`User with the provided ${paramKey} does not exist`);
  }
  return user;
};

const findUserByEmailAndUpdate = async (email, param) => User.findOneAndUpdate(email, param);

const comparePasswords = async (passwordToCompare, currentPassword) => {
  const isPassword = await compare(
    String(passwordToCompare),
    String(currentPassword),
  );

  if (!isPassword) {
    throw new Error('Bad request');
  }

  return isPassword;
};

export {
  saveUser,
  findUserByParam,
  findUserByEmailAndUpdate,
  comparePasswords,
};
