const isShipper = (role) => {
  if (role === 'SHIPPER') {
    return true;
  }
  throw new Error("Available only for shipper's role");
};

const compareLoadParams = (truck, load) => {
  const truckDims = truck.dimensions;
  const loadDims = load.dimensions;

  const compareParams = [
    truck.payload - load.payload,
    truckDims.length - loadDims.length,
    truckDims.width - loadDims.width,
    truckDims.height - loadDims.height,
  ];

  const isLoadFitIntoTruck = compareParams.filter((param) => param < 0);
  return isLoadFitIntoTruck.length === 0;
};

export { isShipper, compareLoadParams };
