import mongoose from 'mongoose';
import Joi from 'joi';

const userJoiSchema = Joi.object({
  role: Joi.string()
    .pattern(/^(DRIVER|SHIPPER)$/)
    .required()
    .error((error) => new Error(error)),

  email: Joi.string()
    .email()
    .required()
    .error((error) => new Error(error)),

  password: Joi.string()
    .min(5)
    .required()
    .error((error) => new Error(error)),
});

const userSchema = new mongoose.Schema(
  {
    role: {
      type: String,
      required: false,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: {
      createdAt: 'created_date',
      updatedAt: false,
    },
    versionKey: false,
  },
);

const User = mongoose.model('User', userSchema);

export { User, userJoiSchema };
