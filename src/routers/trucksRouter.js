import { Router } from 'express';
import {
  trucksAll,
  truckNew,
  truckPersonal,
  truckPersonalTypeChange,
  truckDelete,
  truckAssignById,
} from '../controllers/trucksController.js';
import { asyncWrapper } from '../middleware/asyncWrapper.js';

const router = Router();

router.get('/', asyncWrapper(trucksAll));

router.post('/', asyncWrapper(truckNew));

router.get('/:id', asyncWrapper(truckPersonal));

router.put('/:id', asyncWrapper(truckPersonalTypeChange));

router.delete('/:id', asyncWrapper(truckDelete));

router.post('/:id/assign', asyncWrapper(truckAssignById));

export const trucksRouter = router;
